# batchnamer technical documentation

## About

This is a sample software documentation page for a fictional piece of
software. This page is written solely in HTML and CSS.

[Click here](https://emerac.gitlab.io/batchnamer-documentation)
to view this page!

This page generally follows the requirements as laid out in
freeCodeCamp's Responsive Web Design Course, Project 4. However,
I chose not to follow the requirements exactly so that I could
exercise more creativity.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.

Copyright © 2021 emerac
